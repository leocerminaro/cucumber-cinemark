Feature: Selecionar uma sessão e colocá-la no carrinho
  Scenario: Fluxo feliz
    Given The user is in the movies screen
    And The user has clicked on Ver Mais
    When The user selects a movie
    And The user selects a session
    And The user selects a seat
    And The user selects the ticket type
    And The user clicks on Finalizar Pedido
    And The user clicks on Pular in the dialog
    Then The user will be on the cart
