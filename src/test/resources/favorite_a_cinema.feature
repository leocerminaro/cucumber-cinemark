Feature: Favoritar um cinema
  Scenario: Fluxo feliz
    Given The user is located in the movies page
    When The user clicks on the Cinemas button on the menu
    And the user favorites a Cinema
    Then The user will favorite a Cinema