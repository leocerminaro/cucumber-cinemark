Feature: Logout
    Background:
      Given The user has been logged

      Scenario: Realizar logout com sucesso
        Given the user is in the account screen
        When the user clicks on the Sair button
        Then the user will be redirected to the My Account screen