Feature: Ver dados do usuário
  Background:
    Given the user has logged

  Scenario: Ver dados com sucesso
    Given the user is inside the my account screen
    When the user clicks my data button
    Then the user is redirected to the "My data" screen