Feature: Ver pedidos
  Background:
    Given the user is logged

  Scenario: Ver pedidos com sucesso
  Given the user is in the my account screen
  When the user clicks the orders' button
  Then the user is redirected to the "My Orders" screen