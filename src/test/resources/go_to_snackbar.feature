Feature: Ir para a SnackBar
  Scenario: Fluxo feliz
    Given The user is in the movies page
    When The user clicks on the Snack Bar button on the menu
    And Selects a Cinemark Cinema
    Then The user is going to be redirected to the Snack Bar page