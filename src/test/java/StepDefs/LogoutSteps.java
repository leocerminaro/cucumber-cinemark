package StepDefs;

import Page.BottomNavigationPage;
import Page.LoginPage;
import Page.MoviesPage;
import Page.MyAccountPage;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.lang.*;

public class LogoutSteps {
    LoginPage loginPage = new LoginPage();
    MyAccountPage myAccountPage = new MyAccountPage();
    BottomNavigationPage bottomNavigationPage = new BottomNavigationPage();
    MoviesPage moviesPage = new MoviesPage();
    LoginSteps loginSteps = new LoginSteps();

    @Given("^The user has been logged$")
    public void theUserHasBeenLogged() throws InterruptedException {
        loginSteps.aUserIsInTheLoginScreen();
        loginSteps.theUserInsertsTheCorrectEmailAndPassword();
    }

    @Given("^the user is in the account screen$")
    public void theUserIsInsideTheAccountScreen(){
        myAccountPage.isOnMyAccountPage();
    }

    @When("^the user clicks on the Sair button$")
    public void theUserIsRedirectedToTheMyAccountScreen(){
        myAccountPage.clickOnLogoutButton();
    }

    @Then("the user will be redirected to the My Account screen")
    public void theUserWillBeRedirectedToTheMyAccountScreen(){
        myAccountPage.isOnMyAccountPage();
    }

}
