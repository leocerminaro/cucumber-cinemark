package StepDefs;

import Page.*;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class GoToSnackBarSteps {

    MoviesPage moviesPage = new MoviesPage();
    CinemaPage cinemaPage = new CinemaPage();
    BottomNavigationPage bottomNavigationPage = new BottomNavigationPage();
    SnackBarPage snackBarPage = new SnackBarPage();

    @Given ("^The user is in the movies page$")
    public void theUserIsInTheMoviesPage() throws InterruptedException {
        moviesPage.isOnMoviesScreen();
    }

    @When ("^The user clicks on the Snack Bar button on the menu$")
    public void theUserClicksOnTheSnackBarButtonOnTheMenu(){
        bottomNavigationPage.selectSnackBarBottomNavigationButton();
    }

    @And ("^Selects a Cinemark Cinema$")
    public void selectsACinema(){
        snackBarPage.clickOnSelectACinemarkButton();
        snackBarPage.clickOnSkipTextView();
        snackBarPage.clickOnSearchButton();
        snackBarPage.search("market");
        snackBarPage.clickOnTheFirstCinema();
    }
    @Then ("^The user is going to be redirected to the Snack Bar page$")
    public void theUserIsGoingToBeRedirectedToTheSnackBarPage(){
        snackBarPage.isOnTheSnackBarPage();
    }
}
