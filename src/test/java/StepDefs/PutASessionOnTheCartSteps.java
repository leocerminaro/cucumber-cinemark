package StepDefs;

import Page.*;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PutASessionOnTheCartSteps {
    MoviesPage moviesPage = new MoviesPage();
    SessionTimesPage sessionTimesPage = new SessionTimesPage();
    SeatsPage seatsPage = new SeatsPage();
    PaymentPage paymentPage = new PaymentPage();
    TicketTypePage ticketTypePage = new TicketTypePage();
    CartPage cartPage = new CartPage();

    @Given("^The user is in the movies screen$")
    public void theUserIsInTheMoviesScreen() throws InterruptedException {
        moviesPage.isOnMoviesScreen();
    }

    @And("^The user has clicked on Ver Mais$")
    public void theUserHasClickedOnVerMais(){
        moviesPage.clickOnSeeMore();
    }

    @When("^The user selects a movie$")
    public void theUserSelectsAMovie(){
        moviesPage.selectAMovie();
    }

    @And("^The user selects a session$")
    public void theUserSelectsASession() throws InterruptedException {
        sessionTimesPage.scroll();
        sessionTimesPage.selectASession();
    }

    @And("^The user selects a seat$")
    public void theUserSelectsASeat(){
        seatsPage.getAValidSeat();
    }

    @And("^The user selects the ticket type$")
    public void theUserSelectsTheTicketType(){
        ticketTypePage.selectATicket();
    }

    @And("^The user clicks on Finalizar Pedido$")
    public void theUserClicksProsseguir(){
        ticketTypePage.clicksOnFinalizeOrderButton();
    }

    @And("^The user clicks on Pular in the dialog$")
    public void theUserClicksOnThePaymentScreen(){
        ticketTypePage.clicksOnSkip();
    }

    @Then("^The user will be on the cart$")
    public void theUserWillBeOnTheCart(){
        cartPage.isOnCartPage();
    }

}
