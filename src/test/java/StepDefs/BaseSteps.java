package StepDefs;

import Page.MoviesPage;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import Page.LoginPage;


public class BaseSteps {
    public AppiumDriver driver;
    public TouchAction action;
    protected static String phoneType = "ios";
    public LoginPage loginPage;
    public MoviesPage moviesPage;
   // public OnboardingPage onboarding;
   // public LandingPage landingPage;


    public DesiredCapabilities setDesiredCaps(String phoneType){
        DesiredCapabilities caps = new DesiredCapabilities();
        if (phoneType.equals("android")) {
            caps.setCapability("platformName", "Android");
            caps.setCapability("platformVersion", "7.1.1");
            caps.setCapability("deviceName", "emulator-5554");
            caps.setCapability("app", "/Users/leonardo/Documents/Projetos/Cinemark/cinemark-android/app/build/outputs/apk/debug/app-debug.apk");
            caps.setCapability("newCommandTimeout", 3000);
           caps.setCapability("appWaitActivity", "com.cinemark.presentation.common.MainActivity");
            caps.setCapability("autoGrantPermissions", "true");
            caps.setCapability("automationName", "UiAutomator2");


        }
        else {
            caps.setCapability("platformName", "iOS");
            caps.setCapability("platformVersion", "12.0");
            caps.setCapability("automationName", "XCUITest");
            caps.setCapability("deviceName", "iPhone 8");
            caps.setCapability("bundleID", "br.com.cinemark.iphone");
            caps.setCapability("newCommandTimeout", 3000);
            caps.setCapability("app", "/Users/leonardo/Library/Developer/Xcode/DerivedData/cinemark-ios/Build/Products/Sandbox-iphonesimulator/cinemark-ios.app");
            caps.setCapability("useNewWDA", true);
            caps.setCapability("showXCodeLog", true);
            caps.setCapability("waitForQuiescence", false);
            caps.setCapability("clearSystemFiles", true);
            caps.setCapability("wdaStartupRetryInterval", 1000);
            caps.setCapability("shouldUseSingletonTestManager", false);
            caps.setCapability("noReset", true);
            caps.setCapability("maxTypingFrequency", 30);
        }
        return caps;
    }

    @Before
    public void setup() throws MalformedURLException {
        DesiredCapabilities caps = setDesiredCaps(phoneType = "android");
        driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
        action = new TouchAction(driver);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        moviesPage = new MoviesPage(driver,action,phoneType);

        if(phoneType == "android"){
            //onboarding = new OnboardingPage();
            //landingPage = new LandingPage();
           // onboarding.skipOnboarding();
           // landingPage.toLogin();
        }
    }

    @After
    public void tearDown(Scenario scenario){
        if (scenario.isFailed()){
            try {
                String path;
                path = System.getProperty("user.dir");
                new File(path+"/screenshotFailure").mkdirs();
                FileOutputStream out = new FileOutputStream(path+"/screenshotFailure/"+scenario.getName()+".png");
                byte[] scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                out.write(scrFile);
                scenario.embed(scrFile, "image/png");
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        driver.quit();
    }
}
