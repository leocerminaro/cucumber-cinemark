package StepDefs;

import Page.BottomNavigationPage;
import Page.LoginPage;
import Page.MoviesPage;
import Page.MyAccountPage;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {
    LoginPage loginPage = new LoginPage();
    MyAccountPage myAccountPage = new MyAccountPage();
    BottomNavigationPage bottomNavigationPage = new BottomNavigationPage();
    MoviesPage moviesPage = new MoviesPage();

    @Given ("^The user is in the login screen$")
    public void aUserIsInTheLoginScreen () throws InterruptedException {
        moviesPage.testeUtil();
        bottomNavigationPage.selectProfileBottomNavigationButton();
        myAccountPage.clickOnLoginButton();
        loginPage.isOnModalLoginPage();
    }

    @When("^The user inserts correctly the e-mail and password$")
    public void theUserInsertsTheCorrectEmailAndPassword (){
        loginPage.login("gohan@han.com", "qwe123");
    }

    @Then("^The user is redirected to the my account screen$")
    public void theUserIsRedirectedToTheMyAccountScreen(){
        myAccountPage.isOnMyAccountPage();
    }
}
