package StepDefs;


import Page.LoginPage;
import Page.MyDataPage;
import Page.OrdersPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import Page.MyAccountPage;

public class SeeDataSteps {
    LoginPage loginPage = new LoginPage();
    MyAccountPage myAccountPage = new MyAccountPage();
    MyDataPage myDataPage = new MyDataPage();
    LoginSteps loginSteps = new LoginSteps();

    @Given("^the user has logged$")
    public void theUserHasLogged() throws InterruptedException {
        loginSteps.aUserIsInTheLoginScreen();
        loginSteps.theUserInsertsTheCorrectEmailAndPassword();
    }

    @Given("^the user is inside the my account screen$")
    public void theUserIsInsideTheMyAccountScreen(){
        myAccountPage.isOnMyAccountPage();
    }

    @When("^the user clicks my data button$")
    public void theUserClicksMyDataButton(){
        myAccountPage.clickOnMyDataButton();
    }

    @Then("^the user is redirected to the \"My data\" screen$")
    public void theUserIsRedirectedToTheMyOrdersScreen() throws InterruptedException {
        myDataPage.isOnMyDataPage();
    }
}
