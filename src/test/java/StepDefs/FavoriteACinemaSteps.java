package StepDefs;

import Page.*;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FavoriteACinemaSteps {

    MoviesPage moviesPage = new MoviesPage();
    CinemaPage cinemaPage = new CinemaPage();
    BottomNavigationPage bottomNavigationPage = new BottomNavigationPage();
    SnackBarPage snackBarPage = new SnackBarPage();

    @Given ("^The user is located in the movies page$")
    public void theUserIsLocatedInTheMoviesPage() throws InterruptedException {
        moviesPage.isOnMoviesScreen();
    }

    @When ("^The user clicks on the Cinemas button on the menu$")
    public void theUserClicksOnTheCinemasButtonOnTheMenu(){
        bottomNavigationPage.selectCinemasBottomNavigationButton();
        cinemaPage.isOnCinemaPage();
    }
    @And ("^the user favorites a Cinema$")
    public void theUserFavoritesACinema(){
        cinemaPage.clickOnFavoriteACinemaButton();
    }
    @Then ("^The user will favorite a Cinema$")
     public void theUserWillFavoriteACinema(){
        cinemaPage.thereAreFavoritedCinemas();
    }

}
