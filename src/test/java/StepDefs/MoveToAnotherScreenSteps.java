package StepDefs;

import Page.*;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class MoveToAnotherScreenSteps {
    MoviesPage moviesPage = new MoviesPage();
    CinemaPage cinemaPage = new CinemaPage();
    BottomNavigationPage bottomNavigationPage = new BottomNavigationPage();

    @Given("^The user is in the movies screen of the app$")
    public void theUserIsInTheMoviesScreen() throws InterruptedException {
        moviesPage.isOnMoviesScreen();
    }

    @When("^The user clicks on the Cinemas icon$")
    public void theUserClicksOnTheCinemasIcon(){
        bottomNavigationPage.selectCinemasBottomNavigationButton();
    }

    @Then("^The user will be redirected to the page$")
    public void theUserWillBeRedirectedToThePage() throws InterruptedException {
        cinemaPage.isOnCinemaPage();
    }

}
