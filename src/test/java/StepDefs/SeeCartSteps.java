package StepDefs;

import Page.*;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.lang.*;

public class SeeCartSteps {
    LoginPage loginPage = new LoginPage();
    MyAccountPage myAccountPage = new MyAccountPage();
    BottomNavigationPage bottomNavigationPage = new BottomNavigationPage();
    MoviesPage moviesPage = new MoviesPage();
    LoginSteps loginSteps = new LoginSteps();
    CartPage cartPage = new CartPage();

    @Given("^The user is inside the movies Screen$")
    public void theUserIsInsideTheMoviesScreen() throws InterruptedException {
        moviesPage.isOnMoviesScreen();
    }

    @When("^the user clicks on the cart icon$")
    public void theUserClicksOnTheCartIcon(){
        moviesPage.clickOnTheCartIcon();
    }

    @Then("the user will be redirected to the cart")
    public void theUserWillBeRedirectedToTheCart(){
        cartPage.isOnCartPage();
    }

}
