package StepDefs;


import Page.LoginPage;
import Page.OrdersPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import Page.MyAccountPage;

public class SeeOrdersSteps {
    LoginPage loginPage = new LoginPage();
    MyAccountPage myAccountPage = new MyAccountPage();
    OrdersPage ordersPage = new OrdersPage();

    LoginSteps loginSteps = new LoginSteps();

    @Given("^the user is logged$")
    public void theUserIsLogged() throws InterruptedException {
        loginSteps.aUserIsInTheLoginScreen();
        loginSteps.theUserInsertsTheCorrectEmailAndPassword();
    }

    @Given("^the user is in the my account screen$")
    public void theUserIsInTheMyAccountScreen(){
        myAccountPage.isOnMyAccountPage();
    }

    @When("^the user clicks the orders' button$")
    public void theUserClicksTheOrdersButton(){
        myAccountPage.clickOnMyOrdersButton();
    }

    @Then("^the user is redirected to the \"My Orders\" screen$")
    public void theUserIsRedirectedToTheMyOrdersScreen() throws InterruptedException {
        ordersPage.isOnMyOrdersPage();
    }
}
