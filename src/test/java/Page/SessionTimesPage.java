package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.util.List;

public class SessionTimesPage extends Page{

    @AndroidFindBy(id = "txtviewFilterLabel")
    WebElement filterLabel;

    @AndroidFindBy(id = "rclviewCineSessions")
    List<WebElement> workoutList;

    public SessionTimesPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public SessionTimesPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnSessionTimesPage(){
        return filterLabel.isDisplayed();
    }

    public void selectASession() throws InterruptedException {
        Thread.sleep(5000);
        WebElement firstSession = workoutList.get(0);
        firstSession.click();
    }

    public void scroll()    {
        int centerX = (driver.manage().window().getSize().getWidth()) / 2;
        int centerY = (driver.manage().window().getSize().getHeight()) / 2;
        TouchAction touch = new TouchAction(driver);
        PointOption initialPoint = new PointOption();
        PointOption finalPoint = new PointOption();
        initialPoint.withCoordinates(centerX, centerY);
        finalPoint.withCoordinates(centerX, (centerY - 200));
        touch.longPress(initialPoint).moveTo(finalPoint).release().perform();
    }
}
