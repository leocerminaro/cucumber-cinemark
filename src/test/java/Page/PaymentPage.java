package Page;

import cucumber.api.java.cs.A;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class PaymentPage extends Page{

    //@iOSXCUITFindBy(iOSNsPredicate = "wdValue == \"Cinemas\"")

    @AndroidFindBy(xpath = "//*[@text = \"Pagamento\"]")
    WebElement paymentLabel;

    @AndroidFindBy(xpath = "//*[@resource-id=\"com.cinemark.debug:id/txtlayoutCreditCardNumber\"]/android.widget.FrameLayout[1]")
    WebElement inputCreditCardNumber;

    @AndroidFindBy(xpath = "//*[@resource-id=\"com.cinemark.debug:id/txtlayoutCreditCardName\"]/android.widget.FrameLayout[1]")
    WebElement inputCreditCardName;

    @AndroidFindBy(xpath = "//*[@resource-id=\"com.cinemark.debug:id/txtlayoutCreditCardCpf\"]/android.widget.FrameLayout[1]")
    WebElement inputCPFNumber;

    @AndroidFindBy(xpath = "//*[@resource-id=\"com.cinemark.debug:id/txtlayoutCreditCardDate\"]/android.widget.FrameLayout[1]")
    WebElement inputCreditCardDate;

    @AndroidFindBy(xpath = "//*[@resource-id=\"com.cinemark.debug:id/txtlayoutCreditCardCvv\"]/android.widget.FrameLayout[1]")
    WebElement inputCreditCardCVV;

    @AndroidFindBy(id = "checkboxCreditCardSave")
    WebElement checkboxCreditCardSave;

    @AndroidFindBy(xpath = "//*[@text = \"FINALIZAR PEDIDO\"]")
    WebElement buttonFinalizeOrder;

    @AndroidFindBy(xpath = "//*[@text = \"Confirmar Pagamento?\"]")
    WebElement modalConfirmPaymentLabel;

    @AndroidFindBy(xpath = "//*[@text = \"CONFIRMAR\"]")
    WebElement modalConfirmPaymentButton;

    @AndroidFindBy(xpath = "//*[@text = \"Cancelar\"]")
    WebElement modalCancelPaymentButton;

    @AndroidFindBy(xpath = "//*[@text = \"Cartões cadastrados:\"]")
    WebElement registeredCardsLabel;

    public PaymentPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public PaymentPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnPaymentPage(){
        return paymentLabel.isDisplayed();
    }

}
