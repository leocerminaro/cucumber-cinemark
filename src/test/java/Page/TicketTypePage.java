package Page;

import cucumber.api.java.cs.A;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class TicketTypePage extends Page{


    @AndroidFindBy(xpath = "//*[@text = \"Tipo de Ingresso\"]")
    WebElement ticketTypeLabel;

    @AndroidFindBy(xpath = "//*[@text=\"Inteira\"]")
    WebElement inteiraTicketLabel;

    @AndroidFindBy(id = "com.cinemark.debug:id/rclviewTicket\"]/android.view.ViewGroup[1]/android.widget.LinearLayout[1]/android.widget.ImageView[2]")
    WebElement addTicketInteira;

    @AndroidFindBy(id = "com.cinemark.debug:id/rltlayoutButtonPrice")
    WebElement finalizeButtonTicketTypePage;

    @AndroidFindBy(id = "com.cinemark.debug:id/buttonDialogLeft")
    WebElement dialogSkipButton;


    public TicketTypePage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public TicketTypePage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnTicketTypePage(){
        return ticketTypeLabel.isDisplayed();
    }

    public void selectATicket (){
        addTicketInteira.click();
    }

    public void clicksOnFinalizeOrderButton(){
        finalizeButtonTicketTypePage.click();
    }

    public void clicksOnSkip(){
        dialogSkipButton.click();
    }


}
