package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class SnackBarPage  extends Page{

    @AndroidFindBy(xpath = "//*[@text=\"Snack Bar\"]")
    WebElement snackBarLabel;

    @AndroidFindBy(xpath = "//*[@text=\"Combos\"]")
    WebElement combosButton;

    @AndroidFindBy(xpath = "//*[@text=\"Pipocas\"]")
    WebElement pipocasButton;

    @AndroidFindBy(xpath = "//*[@text=\"Bebidas\"]")
    WebElement bebidasButton;

    @AndroidFindBy(xpath = "//*[@text=\"Salgados\"]")
    WebElement salgadosButton;

    @AndroidFindBy(xpath = "//*[@text=\"Doces\"]")
    WebElement docesButton;

    @AndroidFindBy(xpath = "//*[@text=\"Sorvetes\"]")
    WebElement sorvetesButton;

    @AndroidFindBy(id = "buttonSnackbarSelectCine")
    WebElement selectACinemark;

    @AndroidFindBy(id ="txtviewSkip")
    WebElement skipTextView;

    @AndroidFindBy(id = "search")
    WebElement searchButton;

    @AndroidFindBy(id = "search_src_text")
    WebElement searchTxtField;

    @AndroidFindBy(id = "txtviewCineSelectName")
    WebElement firstCinemaSelected;

    public SnackBarPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public SnackBarPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnTheSnackBarPage(){
        return snackBarLabel.isDisplayed();
    }

    public void clickOnSelectACinemarkButton(){
        selectACinemark.click();
    }

    public void clickOnSkipTextView(){
        skipTextView.click();
    }

    public void clickOnSearchButton(){
        searchButton.click();
    }

    public void search(String cinema){
        searchTxtField.sendKeys(cinema);
        driver.navigate().back();
    }

    public void clickOnTheFirstCinema(){
        firstCinemaSelected.click();
    }
}
