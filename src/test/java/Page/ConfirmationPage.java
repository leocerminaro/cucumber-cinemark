package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class ConfirmationPage extends Page{

    //@iOSXCUITFindBy(iOSNsPredicate = "wdValue == \"Cinemas\"")

    @AndroidFindBy(xpath = "//*[@text = \"Confirmação\"]")
    WebElement confirmationLabel;

    @AndroidFindBy(xpath = "//*[@resource-id=\"com.cinemark.debug:id/fragmentToolbar\"]/android.widget.ImageButton[1]")
    WebElement closeConfirmationDialogButton;

    public ConfirmationPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public ConfirmationPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnSeatsPage(){
        return confirmationLabel.isDisplayed();
    }

}
