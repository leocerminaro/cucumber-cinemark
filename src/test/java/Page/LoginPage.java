package Page;


import cucumber.api.java.en_scouse.An;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import org.apache.tools.ant.taskdefs.Touch;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

import static org.testng.Assert.assertTrue;

public class LoginPage extends Page {



    @iOSXCUITFindBy(iOSNsPredicate = "wdValue == \"E-mail\"")
    @AndroidFindBy(id = "emailEditText")
    WebElement emailField;

    @iOSXCUITFindBy(iOSNsPredicate = "wdValue == \"Senha\"")
    @AndroidFindBy(id = "passwordEditText")
    WebElement passwordField;

    @iOSXCUITFindBy(accessibility = "Entrar")
    @AndroidFindBy(id = "loginButton")
    WebElement modalLoginButton;


    @AndroidFindBy(xpath = "//*[@text=\"Login\"]")
    WebElement modalLoginLabel;

    public LoginPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public LoginPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnModalLoginPage() {
        return modalLoginLabel.isDisplayed();
    }

    public void login (String email, String password) {
        emailField.sendKeys(email);
        passwordField.click();
        passwordField.sendKeys(password);
        driver.navigate().back();
        modalLoginButton.click();
    }
}

