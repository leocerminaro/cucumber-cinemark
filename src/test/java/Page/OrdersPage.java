package Page;


import cucumber.api.java.en_scouse.An;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import org.apache.tools.ant.taskdefs.Touch;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

import static org.testng.Assert.assertTrue;

public class OrdersPage extends Page {

    public OrdersPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public OrdersPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    @AndroidFindBy(xpath = "//*[@text=\"Meus pedidos\"]")
    WebElement myOrdersLabel;

    public void isOnMyOrdersPage() throws InterruptedException {
        Thread.sleep(5000);
        myOrdersLabel.isDisplayed();
    }
}

