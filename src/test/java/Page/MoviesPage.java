package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class MoviesPage extends Page{

    @AndroidFindBy(id = "action_movies")
    WebElement moviesButton;

    @AndroidFindBy(id = "com.cinemark.debug:id/seeMore")
    WebElement seeMoreMoviesPage;

    @AndroidFindBy(xpath = "//*[@text=\"A Abelhinha Maya\"]")
    WebElement firstMovie;

    @AndroidFindBy(id = "iv_banner_sp")
    WebElement bannerSP;

    @AndroidFindBy(id = "shoppingCart")
    WebElement cartIcon;

    @AndroidFindBy(id = "flowContainer")
    WebElement flowContainer;

    public MoviesPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public boolean testeUtil() throws InterruptedException{
        Thread.sleep(5000);
        if(bannerSP.isDisplayed())
            driver.navigate().back();
        return moviesButton.isSelected();
    }
    public MoviesPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public void isOnMoviesScreen() throws InterruptedException {
       testeUtil();
    }

    public void selectAMovie(){
        firstMovie.click();
    }

    public void clickOnSeeMore(){
        seeMoreMoviesPage.click();
    }

    public void clickOnTheCartIcon(){
        cartIcon.click();
    }


}
