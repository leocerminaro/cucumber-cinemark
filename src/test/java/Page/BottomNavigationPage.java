package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class BottomNavigationPage extends Page{

        @AndroidFindBy(id = "action_movies")
        WebElement moviesBottomNavigationButton;

        @AndroidFindBy(id = "action_cine")
        WebElement cinemasBottomNavigationButton;

        @AndroidFindBy(id = "action_snackbar")
        WebElement snackBarBottomNavigationButton;

        @AndroidFindBy(id = "action_profile")
        WebElement profileBottomNavigationButton;

        public BottomNavigationPage(AppiumDriver driver, TouchAction action, String phoneType){
            super.setStaticAtributtes(driver,action,phoneType);
        }

        public BottomNavigationPage (){
            driver = super.getDriver();
            action = super.getAction();
            phoneType = super.getPhoneType();
            PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
        }

        public void selectMoviesBottomNavigationButton(){
            moviesBottomNavigationButton.click();
        }

        public void selectCinemasBottomNavigationButton(){
            cinemasBottomNavigationButton.click();
        }

        public void selectSnackBarBottomNavigationButton(){
            snackBarBottomNavigationButton.click();
        }

        public void selectProfileBottomNavigationButton(){
            profileBottomNavigationButton.click();
        }

    }

