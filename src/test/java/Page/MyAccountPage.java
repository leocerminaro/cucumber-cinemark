package Page;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class MyAccountPage extends Page {

    @iOSXCUITFindBy(xpath = "//*[@name=\"FAZER LOGIN\"]")
    @AndroidFindBy(id = "buttonLoginProfileNotLogged")
    WebElement fazerLoginButton;

    @iOSXCUITFindBy(iOSNsPredicate = "wdValue == \"Minha Conta\"")
    @AndroidFindBy(xpath = "//*[@resource-id=\"com.cinemark.debug:id/largeLabel\"]")
    WebElement myAccountLabel;

    @iOSXCUITFindBy(iOSNsPredicate = "wdValue == \"Pedidos\"")
    @AndroidFindBy(id = "rltlayoutProfileMyOrders")
    WebElement ordersButton;

    @iOSXCUITFindBy(iOSNsPredicate = "wdValue == \"Alterar senha\"")
    @AndroidFindBy(id = "rltlayoutPassswordProfileLogged")
    WebElement changePasswordButton;

    @AndroidFindBy(id = "rltlayoutMyDataProfileLogged")
    WebElement myDataButton;


    @AndroidFindBy(id = "txtviewLogoutProfileLogged")
    WebElement logoutButton;

    public MyAccountPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public MyAccountPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnMyAccountPage(){
        return myAccountLabel.isDisplayed();
    }

    public void clickOnMyOrdersButton (){
        ordersButton.click();
    }

    public void clickOnLoginButton(){
        fazerLoginButton.click();
    }

    public void clickOnMyDataButton(){
        myDataButton.click();
    }

    public void clickOnLogoutButton(){
        logoutButton.click();
    }
}
