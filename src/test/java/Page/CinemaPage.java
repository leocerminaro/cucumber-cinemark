package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class CinemaPage extends Page{

    @iOSXCUITFindBy(iOSNsPredicate = "wdValue == \"Cinemas\"")
    @AndroidFindBy(id = "action_cine")
    WebElement cinemasLabel;

    @AndroidFindBy(id ="txtviewSkip")
    WebElement skipTextView;

    @AndroidFindBy(id = "imgviewCineSelectFavorite")
    WebElement favoriteACinemaButton;

    @AndroidFindBy(xpath = "//*[@text=\"Cinemas favoritos\"]")
    WebElement favoriteCinemasLabel;

    @AndroidFindBy(xpath = "//*[@text=\"Todos os cinemas (por localização):\"]")
    WebElement testeScrollElement;

    public CinemaPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public CinemaPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnCinemaPage(){
        skipTextView.click();
        return cinemasLabel.isDisplayed();
    }

    public void clickOnFavoriteACinemaButton(){
        favoriteACinemaButton.click();
    }

    public void thereAreFavoritedCinemas(){
        favoriteCinemasLabel.isDisplayed();
    }



}
