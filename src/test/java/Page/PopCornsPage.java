package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class PopCornsPage extends Page{

    @AndroidFindBy(xpath = "//*[@text=\"Pipocas\"]")
    WebElement popCornsLabel;

    @AndroidFindBy(xpath = "//*[@resource-id=\"//*[@resource-id=\"com.cinemark.debug:id/rclviewProductList\"]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]")
    WebElement firstIceCream;

    @AndroidFindBy(id = "addToCartButton")
    WebElement addToCartButton;

    public PopCornsPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public PopCornsPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnPopCornsPage(){
        return popCornsLabel.isDisplayed();
    }
}
