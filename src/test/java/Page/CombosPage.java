package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class CombosPage extends Page{

    @AndroidFindBy(xpath = "//*[@text=\"Combos\"]")
    WebElement combosLabel;

    @AndroidFindBy(xpath = "//*[@resource-id=\"com.cinemark.debug:id/rclviewProductList\"]/android.widget.LinearLayout[1]")
    WebElement firstCombo;

    @AndroidFindBy(xpath = "//*[@resource-id=\"com.cinemark.debug:id/rclviewProductDetailList\"]/android.widget.RelativeLayout[1]/android.widget.ImageView[3]")
    WebElement firstComboElement;

    @AndroidFindBy(id = "addToCartButton")
    WebElement addToCartButton;

    @AndroidFindBy(id = "shoppingCart")
    WebElement cartIcon;
    public CombosPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public CombosPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnCombosPage(){
        return combosLabel.isDisplayed();
    }

    public void selectACombo(){
        firstCombo.click();
    }

}
