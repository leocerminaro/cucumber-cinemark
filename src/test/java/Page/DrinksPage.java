package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class DrinksPage extends Page{

    @AndroidFindBy(xpath = "//*[@text=\"Bebidas\"]")
    WebElement drinksLabel;

    @AndroidFindBy(xpath = "//*[@resource-id=\"//*[@resource-id=\"com.cinemark.debug:id/rclviewProductList\"]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]")
    WebElement firstDrink;

    @AndroidFindBy(id = "addToCartButton")
    WebElement addToCartButton;

    public DrinksPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public DrinksPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

    public boolean isOnDrinksPage(){
        return drinksLabel.isDisplayed();
    }
}
