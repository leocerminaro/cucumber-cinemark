package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.util.Locale;
import java.util.ResourceBundle;

public class Page {
    public static AppiumDriver driver;
    public static TouchAction action;
    public static String phoneType;

    Utils utils = new Utils();
    public static void setStaticAtributtes(AppiumDriver driver, TouchAction action, String phoneType){
        Page.driver = driver;
        Page.action = action;
        Page.phoneType = phoneType;
    }

    public AppiumDriver getDriver(){
        return driver;
    }

    public TouchAction getAction() {
        return action;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setWorkspace(AppiumDriver driver, TouchAction action, String phoneType){
        this.setStaticAtributtes(driver, action, phoneType);

    }

}
