package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class SeatsPage extends Page{

        // @iOSXCUITFindBy(iOSNsPredicate = "wdValue == \"Cinemas\"")

        @AndroidFindBy(xpath = "//*[@text = \"Escolha seu assento\"]")
        WebElement selectYourSeatLabel;

        @AndroidFindBy(id = "298636")
        WebElement firstSeat;
        int seat;
        public SeatsPage(AppiumDriver driver, TouchAction action, String phoneType){
            super.setStaticAtributtes(driver,action,phoneType);
        }

        public SeatsPage (){
            driver = super.getDriver();
            action = super.getAction();
            phoneType = super.getPhoneType();
            PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
        }

        public boolean isOnSeatsPage(){
            return selectYourSeatLabel.isDisplayed();
        }

        public void getAValidSeat(){
                firstSeat.click();
            }
}

