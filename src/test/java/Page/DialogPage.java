package Page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class DialogPage extends Page{

    WebElement inteiraTicketLabel;

    @AndroidFindBy(xpath = "//*[@text = \"Que tal uma pipoca para acompanhar o filme? Comprando aqui você não perde tempo, é só retirar o seu pedido!\"]")
    WebElement dialogPostFinalizeOrder;

    @AndroidFindBy(xpath = "//*[@text = \"PULAR\"]")
    WebElement skipDialogButton;

    @AndroidFindBy(xpath = "//*[@text = \"COMPRAR\"]")
    WebElement buyDialogButton;

    public DialogPage(AppiumDriver driver, TouchAction action, String phoneType){
        super.setStaticAtributtes(driver,action,phoneType);
    }

    public DialogPage (){
        driver = super.getDriver();
        action = super.getAction();
        phoneType = super.getPhoneType();
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(utils.getTimeout())), this);
    }

}
